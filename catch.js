const grid = document.getElementById('catch-grid');
const nodes = document.getElementsByTagName('div');

let headCoord = 0;
let itemCoord = 0;
	for (i = 0;i < (21*21);i++) {
         grid.innerHTML += "<div></div>"
      }


//COUNTDOWN
let countdown = 10;
    setInterval(function(){
      console.log(countdown)
      countdown--;
      countdownView();
      gameOver();
      }, 1000);

let score = -1;

let countdownView = function () {
        document.getElementById("countdown").innerHTML = countdown;
      }

let scoreView = function () {
        document.getElementById("score").innerHTML = score;
        document.getElementById("final-score").innerHTML = score;
      }

let gameOver = function() {
   if (countdown <= 0) {
   grid.style.display = "none"
   document.getElementById("statistics").style.display = "none"
   document.getElementById("game-over-message").style.display = "block"
        }
      }
      
//CONTROLS
document.addEventListener('keydown', function(event) {
    if(event.keyCode == 37) {
          moveLeft();
    }
    else if(event.keyCode == 38) {
          moveUp();
    }
    else if(event.keyCode == 39) {
          moveRight();
    }
    else if(event.keyCode == 40) {
          moveDown();
    }
});


let moveLeft = function() {
   prevCoord = headCoord;
   headCoord --;
   moveCoord(prevCoord, headCoord);
   }

let moveUp = function() {
   prevCoord = headCoord;
   headCoord -= 21;
   moveCoord (prevCoord, headCoord);
   }

let moveRight = function() {
   prevCoord = headCoord;
   headCoord ++ ;
   moveCoord(prevCoord, headCoord);
   }

let moveDown = function() {
   prevCoord = headCoord;
   headCoord += 21;
   moveCoord(prevCoord, headCoord);
   }

let moveCoord = function (prev, current) {
    nodes[prev].style.backgroundColor = "transparent";
    nodes[current].style.backgroundColor = "black";
     eatFruit();
 	}

let randomCoord = function() {
        return Math.ceil(Math.random() * (21*21));
      }


let placeItem = function() {
    itemCoord = randomCoord();
    nodes[itemCoord].style.backgroundColor = "red"
    score++;
    scoreView();
    countdown = 10;
    countdownView();
    }
	
	placeItem();


let eatFruit = function() {
    if(headCoord === itemCoord) {
    	console.log("GOTCHA")
       	placeItem();
     	}
    }





//CONTROLS